#include <QApplication>
#include <QQmlApplicationEngine>

#include "fortuneclient.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<FortuneClient>("Fortune", 1, 0, "FortuneClient");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
