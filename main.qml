import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

import Fortune 1.0

ApplicationWindow {
    title: qsTr("Fortune Cookie");
    width: 640;
    height: 480;
    visible: true;

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Application");
            MenuItem {
                text: qsTr("&Set Server Details...");
                onTriggered: serverDialog.show(qsTr("Open action triggered"));
            }
            MenuItem {
                text: qsTr("E&xit");
                onTriggered: Qt.quit();
            }
        }
    }

    statusBar: StatusBar {
        RowLayout {
            anchors.fill: parent;
            Label {
                id: statusBarText;
                text: qsTr("Set server details in the Application menu");
            }
        }
    }

    Item {
        id: mainForm;
        anchors.fill: parent;

        GridLayout {
            rows: 2;
            columns: 1;
            rowSpacing: 32;

            anchors.centerIn: parent;

            Label {
                id: mainFortuneLabel;
                Layout.maximumWidth: 600;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter;

                text: qsTr("Set a server, and click the Get Fortune button");
                wrapMode: Text.Wrap;
                horizontalAlignment: Text.AlignHCenter;
                font.pointSize: 36;

                function setText(mtext) {
                    text = qsTr(mtext);
                    horizontalAlignment = Text.AlignLeft;
                    horizontalAlignment = Text.AlignHCenter;
                }
            }

            Button {
                id: getFortuneButton;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter;

                text: "Get Fortune";
                onClicked: fortuneClient.getNewFortune();
            }
        }
    }

    Dialog {
        id: serverDialog
        width: 384;
        height: 128;
        title: qsTr("Server Settings");

        GridLayout {
            rows: 2;
            columns: 2;

            anchors.centerIn: parent;

            Label {
                Layout.row: 1;
                Layout.column: 1;
                Layout.preferredWidth: 32;
                Layout.fillWidth: true;

                text: qsTr("Server:");
                horizontalAlignment: Text.AlignRight;
            }

            Label {
                Layout.row: 2;
                Layout.column: 1;
                Layout.preferredWidth: 32;
                Layout.fillWidth: true;

                text: qsTr("Port:");
                horizontalAlignment: Text.AlignRight;
            }

            TextField {
                Layout.row: 1;
                Layout.column: 2;
                Layout.preferredWidth: 256;

                id: serverTextField;
                placeholderText: qsTr("Enter server hostname or IP address");
            }

            TextField {
                Layout.row: 2;
                Layout.column: 2;
                Layout.preferredWidth: 256;

                id: portTextField;
                placeholderText: qsTr("Enter server port");
                validator: IntValidator { bottom: 0; top: 65535; }
            }

        }

        onAccepted: fortuneClient.setServerPort();

        function show(caption) {
            serverDialog.open();
        }
    }

    FortuneClient {
        id: fortuneClient;
        onHaveFortune: mainFortuneLabel.setText(fortune);

        function setServerPort() {
            serverHost = serverTextField.text;
            serverPort = portTextField.text;

            statusBarText.text = qsTr("OK: Server set to %1:%2".arg(serverHost).arg(serverPort));
        }
    }
}
