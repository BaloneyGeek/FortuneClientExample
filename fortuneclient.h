#ifndef FORTUNECLIENT_H
#define FORTUNECLIENT_H

#include <QQuickItem>
#include <QString>
#include <QByteArray>
#include <QTcpSocket>

class FortuneClient : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString serverHost READ serverHost WRITE setServerHost)
    Q_PROPERTY(int     serverPort READ serverPort WRITE setServerPort)

    private:

    quint16 mPort;
    QString mHost;

    QTcpSocket * clientSocket;

    public:

    FortuneClient();
    ~FortuneClient();

    QString serverHost() const;
    void setServerHost(QString host);
    quint16 serverPort() const;
    void setServerPort(int port);

    signals:

    void haveFortune(QString fortune);

    public slots:

    void getNewFortune();
};

#endif // FORTUNECLIENT_H
