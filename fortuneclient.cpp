#include "fortuneclient.h"

FortuneClient::FortuneClient()
{
    mPort = 0;
    clientSocket = new QTcpSocket(this);
}

FortuneClient::~FortuneClient()
{
    clientSocket->disconnectFromHost();
    clientSocket->deleteLater();
}

/* accessors and mutators for the properties go first */

QString FortuneClient::serverHost() const
{
    return mHost;
}

void FortuneClient::setServerHost(QString host)
{
    mHost = host;
}

quint16 FortuneClient::serverPort() const
{
    return mPort;
}

void FortuneClient::setServerPort(int port)
{
    mPort = (quint16)port;
}

/* the slot we call to get a new fortune */

void FortuneClient::getNewFortune()
{
    QByteArray fortune;

    clientSocket->connectToHost(mHost, mPort, QIODevice::ReadOnly);
    clientSocket->waitForReadyRead();
    fortune = clientSocket->readAll();
    clientSocket->disconnectFromHost();

    emit haveFortune(QString(fortune));
}

